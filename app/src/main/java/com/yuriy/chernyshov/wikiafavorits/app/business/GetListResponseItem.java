package com.yuriy.chernyshov.wikiafavorits.app.business;

import java.io.Serializable;
import java.util.List;

/**
 * Created at Android Studio
 * Author: Yuriy Chernyshov
 * Date: 01.07.14
 * Time: 20:51
 */
public class GetListResponseItem implements Serializable {

    private List items;
    private int currentBatch;
    private int batchesNumber;

    public List getItems() {
        return items;
    }

    public void setItems(List items) {
        this.items = items;
    }

    public int getCurrentBatch() {
        return currentBatch;
    }

    public void setCurrentBatch(int currentBatch) {
        this.currentBatch = currentBatch;
    }

    public int getBatchesNumber() {
        return batchesNumber;
    }

    public void setBatchesNumber(int batchesNumber) {
        this.batchesNumber = batchesNumber;
    }
}