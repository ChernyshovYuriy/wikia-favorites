package com.yuriy.chernyshov.wikiafavorits.app.api;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 5/17/14
 * Time: 12:41 AM
 */

/**
 * Enumeration of the Hub values
 */
public enum Hub {
    GAMING,
    ENTERTAINMENT,
    LIFESTYLE;

    /**
     * Convert enum value into String
     * @param value Enum value
     * @return String value of the provided enum value
     */
    public static String stringValueOf(Hub value) {
        switch (value) {
            case GAMING:
                return "Gaming";
            case ENTERTAINMENT:
                return "Entertainment";
            case LIFESTYLE:
                return "Lifestyle";
        }
        return "";
    }

    @Override
    public String toString() {
        return super.toString();
    }
}