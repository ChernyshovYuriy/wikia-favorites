package com.yuriy.chernyshov.wikiafavorits.app.net;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 5/17/14
 * Time: 12:38 AM
 */
public class URLBuilder {

    private static final String DATA_URL = "http://www.wikia.com/wikia.php";

    private String mIds = "";
    private String mMethod = "";
    private String mHub = "";
    private String mController = "";
    private String mLang = "";
    private int mLimit = 25;
    private int mBatchId = 1;
    private int mHeight = 0;
    private int mWidth = 0;
    private boolean mDoExpand = false;

    public void setLang(String mLang) {
        this.mLang = mLang;
    }

    public void setController(String mController) {
        this.mController = mController;
    }

    public void setMethod(String mMethod) {
        this.mMethod = mMethod;
    }

    public void setDoExpand(boolean mDoExpand) {
        this.mDoExpand = mDoExpand;
    }

    public void setLimit(int mLimit) {
        this.mLimit = mLimit;
    }

    public void setBatchId(int mBatchId) {
        this.mBatchId = mBatchId;
    }

    public void setHub(String mHub) {
        this.mHub = mHub;
    }

    public void setIds(String mIds) {
        this.mIds = mIds;
    }

    public void setHeight(int mHeight) {
        this.mHeight = mHeight;
    }

    public void setWidth(int mWidth) {
        this.mWidth = mWidth;
    }

    public String buildURL() {
        StringBuilder builder = new StringBuilder(DATA_URL);
        builder.append("?controller=").append(mController);
        builder.append("&method=").append(mMethod);
        builder.append("&limit=").append(mLimit);
        builder.append("&hub=").append(mHub);
        builder.append("&batch=").append(mBatchId);
        if (!mIds.equals("")) {
            builder.append("&ids=").append(mIds);
        }
        if (mHeight > 0) {
            builder.append("&height=").append(mHeight);
        }
        if (mWidth > 0) {
            builder.append("&width=").append(mWidth);
        }
        if (mDoExpand) {
            builder.append("&expand=yes");
        }
        builder.append("&lang=").append(mLang);
        return builder.toString();
    }
}