package com.yuriy.chernyshov.wikiafavorits.app;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.yuriy.chernyshov.wikiafavorits.app.api.APIMethod;
import com.yuriy.chernyshov.wikiafavorits.app.business.GetListResponseItem;
import com.yuriy.chernyshov.wikiafavorits.app.list.ListAdapter;
import com.yuriy.chernyshov.wikiafavorits.app.list.ListAdapterData;
import com.yuriy.chernyshov.wikiafavorits.app.list.ListItem;
import com.yuriy.chernyshov.wikiafavorits.app.service.DownloaderService;
import com.yuriy.chernyshov.wikiafavorits.app.utils.AppUtilities;
import com.yuriy.chernyshov.wikiafavorits.app.utils.Constants;
import com.yuriy.chernyshov.wikiafavorits.app.utils.ImageFetcher;
import com.yuriy.chernyshov.wikiafavorits.app.utils.ImageFetcherFactory;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Main application View
 */
public class MainActivity extends ActionBarActivity /*implements IDataRunnableListener*/ {

    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    private static final String CURRENT_BATCH_KEY = "CURRENT_BATCH_KEY";
    private static final String TOTAL_BATCHES_KEY = "TOTAL_BATCHES_KEY";
    private static final String ADAPTER_DATA_KEY = "ADAPTER_DATA_KEY";

    private ListAdapter mListAdapter;
    private ImageFetcher mImageFetcher; // Handles loading the  image in a background thread

    private int mCurrentBatch = 1;
    private int mBatchesNumber = 0;

    private boolean mIsLoadingMoreInProgress = false;

    /**
     * Instantiate the {@link com.yuriy.chernyshov.wikiafavorits.app.MainActivity.MessengerHandler},
     * passing in the {@link com.yuriy.chernyshov.wikiafavorits.app.MainActivity} to be stored
     * as a WeakReference
     */
    private final MessengerHandler handler = new MessengerHandler(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(LOG_TAG, "OnCreate:"+ savedInstanceState);

        setContentView(R.layout.activity_main);

        mImageFetcher = ImageFetcherFactory.getSmallImageFetcher(this);

        // Create and set empty adapter
        mListAdapter = new ListAdapter(this, mImageFetcher);

        initializeListView();

        if (savedInstanceState == null) {

            downloadBatch(mCurrentBatch);

            return;
        }

        mCurrentBatch = savedInstanceState.getInt(CURRENT_BATCH_KEY);
        mBatchesNumber = savedInstanceState.getInt(TOTAL_BATCHES_KEY);
        ListAdapterData<ListItem> listItems =
                (ListAdapterData<ListItem>) savedInstanceState.getSerializable(ADAPTER_DATA_KEY);
        if (listItems == null) {
            return;
        }
        setListAdapterData(listItems.getItems());
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.i(LOG_TAG, "OnResume");

        mImageFetcher.setExitTasksEarly(false);
        mListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();

        Log.i(LOG_TAG, "OnPause");

        mImageFetcher.setPauseWork(false);
        mImageFetcher.setExitTasksEarly(true);
        mImageFetcher.flushCache();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Log.i(LOG_TAG, "OnSaveInstanceState:" + outState);

        outState.putInt(CURRENT_BATCH_KEY, mCurrentBatch);
        outState.putInt(TOTAL_BATCHES_KEY, mBatchesNumber);
        outState.putSerializable(ADAPTER_DATA_KEY, mListAdapter.getData());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.i(LOG_TAG, "OnDestroy");

        mImageFetcher.closeCache();
    }

    private void downloadBatch(int batchId) {

        showProgressBar();

        Intent intent = DownloaderService.makeIntent(this, handler);
        intent.putExtra(Constants.API_METHOD_KEY, APIMethod.GET_LIST);
        intent.putExtra(Constants.BATCH_ID_KEY, batchId);

        startService(intent);
    }

    private void initializeListView() {
        ListView listView = (ListView) findViewById(R.id.list_view);
        listView.setAdapter(mListAdapter);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // Pause fetcher to ensure smoother scrolling when flinging
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
                    // Before Honeycomb pause image loading on scroll to help with performance
                    if (!AppUtilities.hasHoneycomb()) {
                        mImageFetcher.setPauseWork(true);
                    }
                } else {
                    mImageFetcher.setPauseWork(false);
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                                 int totalItemCount) {
                //Log.d(LOG_TAG, "On Scroll, first item:" + firstVisibleItem + ", visible count:" +
                //    visibleItemCount + ", total count:" + totalItemCount);

                if (firstVisibleItem + visibleItemCount == totalItemCount) {
                    if (mCurrentBatch < mBatchesNumber) {

                        // Do not load more data if the loading procedure is in progress
                        if (mIsLoadingMoreInProgress) {
                            return;
                        }
                        mIsLoadingMoreInProgress = true;

                        downloadBatch(mCurrentBatch + 1);
                    }
                }
            }
        });
        /*listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // DialogFragment.show() will take care of adding the fragment
                // in a transaction.  We also want to remove any currently showing
                // dialog, so make our own transaction and take care of that here.
                FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                        .beginTransaction();
                Fragment prevFragment = getSupportFragmentManager()
                        .findFragmentByTag(DETAILS_VIEW_FRAGMENT_TAG);
                if (prevFragment != null) {
                    fragmentTransaction.remove(prevFragment);
                }
                fragmentTransaction.addToBackStack(null);

                // Create and show the dialog.
                DialogFragment dialogFragment = DetailsView.newInstance(position);
                dialogFragment.show(fragmentTransaction, DETAILS_VIEW_FRAGMENT_TAG);
            }
        });*/
    }

    private void setListAdapterData(final List<ListItem> data) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mListAdapter.addItems(data);
                mListAdapter.notifyDataSetChanged();
            }
        });
    }

    /**
     * Return a {@link com.yuriy.chernyshov.wikiafavorits.app.list.ListItem} at the specified
     * position
     * @param index position in the collection
     * @return {@link com.yuriy.chernyshov.wikiafavorits.app.list.ListItem}
     */
    /*public ListItem getListItemAt(int index) {
        return (ListItem) mListAdapter.getItem(index);
    }*/

    /**
     * Show Progress Bar
     */
    private void showProgressBar() {
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar_view);
        progressBar.setVisibility(View.VISIBLE);
    }

    /**
     * Hide Progress Bar
     */
    private void hideProgressBar() {
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar_view);
        progressBar.setVisibility(View.GONE);
    }

    /**
     * This is the handler used for handling messages sent by a
     * Messenger.  It receives a message containing a pathname to an
     * image and displays that image in the ImageView.
     *
     * The handler plays several roles in the Active Object pattern,
     * including Proxy, Future, and Servant.
     *
     * Please use displayBitmap() defined in DownloadBase
     */
    static class MessengerHandler extends Handler {

        // A weak reference to the enclosing class
        WeakReference<MainActivity> outerClass;

        /**
         * A constructor that gets a weak reference to the enclosing class.
         * We do this to avoid memory leaks during Java Garbage Collection.
         *
         * @see <a href="https://groups.google.com/forum/#!msg/android-developers/1aPZXZG6kWk/lIYDavGYn5UJ">Stackoverflow's answer</a>
         */
        public MessengerHandler(MainActivity outer) {
            outerClass = new WeakReference<MainActivity>(outer);
        }

        // Handle any messages that get sent to this Handler
        @Override
        public void handleMessage(Message msg) {

            // Get an actual reference to the MainActivity from the WeakReference.
            final MainActivity activity = outerClass.get();

            // If DownloadActivity hasn't been garbage collected
            // (closed by user), display the sent image.
            if (activity == null) {
                return;
            }

            activity.mIsLoadingMoreInProgress = false;
            activity.hideProgressBar();

            switch (msg.what) {
                case Constants.GET_LIST_RESPONSE_MESSENGER_KEY:
                    GetListResponseItem responseItem =
                            (GetListResponseItem) msg.getData().getSerializable(
                                    Constants.GET_LIST_RESPONSE_ITEM_KEY);

                    if (responseItem == null) {
                        return;
                    }

                    Log.d(LOG_TAG, "Current batch:" + responseItem.getCurrentBatch());
                    Log.d(LOG_TAG, "Batches number:" + responseItem.getBatchesNumber());
                    Log.d(LOG_TAG, "Items:" + responseItem.getItems().size());

                    activity.mCurrentBatch = responseItem.getCurrentBatch();
                    activity.mBatchesNumber = responseItem.getBatchesNumber();
                    activity.setListAdapterData(responseItem.getItems());
                    break;
            }
        }
    }
}