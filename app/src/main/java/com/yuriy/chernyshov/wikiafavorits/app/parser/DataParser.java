package com.yuriy.chernyshov.wikiafavorits.app.parser;

import com.yuriy.chernyshov.wikiafavorits.app.list.ListItem;

import java.util.List;

/**
 * Created at Android Studio
 * Author: Yuriy Chernyshov
 * Date: 01.07.14
 * Time: 22:44
 */
public abstract class DataParser {

    /**
     * Define JSON data keys
     */
    protected static final String KEY_BATCHES = "batches";
    protected static final String KEY_CURRENT_BATCH = "currentBatch";
    protected static final String KEY_ITEMS = "items";
    protected static final String KEY_ID = "id";
    protected static final String KEY_NAME = "name";
    protected static final String KEY_HUB = "hub";
    protected static final String KEY_LANGUAGE = "language";
    protected static final String KEY_TOPIC = "topic";
    protected static final String KEY_DOMAIN = "domain";
    protected static final String KEY_WORDMARK = "wordmark";
    protected static final String KEY_TITLE = "title";
    protected static final String KEY_URL = "url";
    protected static final String KEY_STATS = "stats";
    protected static final String KEY_EDITS = "edits";
    protected static final String KEY_ARTICLES = "articles";
    protected static final String KEY_PAGES = "pages";
    protected static final String KEY_USERS = "users";
    protected static final String KEY_ACTIVE_USERS = "activeUsers";
    protected static final String KEY_IMAGES = "images";
    protected static final String KEY_VIDEOS = "videos";
    protected static final String KEY_ADMINS = "admins";
    protected static final String KEY_TOP_USERS = "topUsers";
    protected static final String KEY_HEAD_LINE = "headline";
    protected static final String KEY_LANG = "lang";
    protected static final String KEY_FLAGS = "flags";
    protected static final String KEY_DESC = "desc";
    protected static final String KEY_IMAGE = "image";
    protected static final String KEY_WAM_SCORE = "wam_score";
    protected static final String KEY_ORIGINAL_DIMENSIONS = "original_dimensions";
    protected static final String KEY_WIDTH = "width";
    protected static final String KEY_HEIGHT = "height";

    public abstract int parseBatchesNumber(String inputData);

    public abstract int parseCurrentBatch(String inputData);

    public abstract List<ListItem> parseItemsFromArray(String inputData);
}