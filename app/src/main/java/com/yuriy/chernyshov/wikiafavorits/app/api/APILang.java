package com.yuriy.chernyshov.wikiafavorits.app.api;

/**
 * Created at Android Studio
 * Author: Yuriy Chernyshov
 * Date: 01.07.14
 * Time: 22:54
 */
public enum APILang {

    ENG;

    /**
     * Convert enum value into String
     * @param value Enum value
     * @return String value of the provided enum value
     */
    public static String stringValueOf(APILang value) {
        switch (value) {
            case ENG:
                return "en";
        }
        return "";
    }

    @Override
    public String toString() {
        return super.toString();
    }
}