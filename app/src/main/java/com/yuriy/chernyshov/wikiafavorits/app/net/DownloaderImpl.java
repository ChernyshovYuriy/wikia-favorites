package com.yuriy.chernyshov.wikiafavorits.app.net;

import android.net.Uri;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * Created at Android Studio
 * Author: Yuriy Chernyshov
 * Date: 01.07.14
 * Time: 22:39
 */
public class DownloaderImpl implements Downloader {

    private static final String LOG_TAG = DownloaderImpl.class.getSimpleName();

    @Override
    public String downloadFromUri(Uri uri) {
        HttpGet request = new HttpGet(uri.toString());
        HttpClient httpClient = new DefaultHttpClient();
        try {
            HttpResponse httpResponse = httpClient.execute(request);
            //Log.d(LOG_TAG, "Response code: " + httpResponse.getStatusLine().getStatusCode());
            int responseCode = httpResponse.getStatusLine().getStatusCode();
            if (responseCode == 200) {
                HttpEntity entity = httpResponse.getEntity();
                if (entity != null) {
                    try {
                        return EntityUtils.toString(entity);
                    } catch (IOException e) {
                        Log.e(LOG_TAG, "EntityUtils error: " + e.getMessage());
                    }
                }
            }
        } catch (ClientProtocolException e) {
            Log.e(LOG_TAG, "ClientProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(LOG_TAG, "IOException: " + e.getMessage());
        }
        return "";
    }
}