package com.yuriy.chernyshov.wikiafavorits.app.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

/**
 * Created at Android Studio
 * Author: Yuriy Chernyshov
 * Date: 29.06.14
 * Time: 18:01
 */
public class DownloadUtils {

    /**
     * The key used to store/retrieve a Messenger extra from a Bundle.
     */
    public static final String MESSENGER_KEY = "MESSENGER";

    /**
     * The key used to store/retrieve a downloaded data from a Bundle.
     */
    public static final String DOWNLOADED_DATA_KEY = "DOWNLOADED_DATA_KEY";

    @SuppressWarnings("unused")
    private static final String LOG_TAG = DownloadUtils.class.getSimpleName();

    /**
     * Make an Intent which will start a service if provided as a
     * parameter to startService().
     *
     * @param context		The context of the calling component
     * @param service		The class of the service to be
     *                          started. (For example, ThreadPoolDownloadService.class)
     * @param handler		The handler that the service should
     *                          use to return a result.
     *
     * This method is an example of the Factory Method Pattern,
     * because it creates and returns a different Intent depending on
     * the parameters provided to it.
     *
     * The Intent is used as the Command Request in the Command
     * Processor Pattern when it is passed to the
     * ThreadPoolDownloadService using startService().
     *
     * The Handler is used as the Proxy, Future, and Servant in the
     * Active Object Pattern when it is passed a message, which serves
     * as the Active Object, and acts depending on what the message
     * contains.
     *
     * The handler *must* be wrapped in a Messenger to allow it to
     * operate across process boundaries.
     */
    public static Intent makeMessengerIntent(Context context,
                                             Class<?> service,
                                             Handler handler) {
        Messenger messenger = new Messenger(handler);

        Intent intent = new Intent(context, service);
        intent.putExtra(MESSENGER_KEY, messenger);

        return intent;
    }

    /**
     * Download a file to the Android file system, then respond with
     * the file location using the provided Messenger.
     */
    public static void downloadAndRespond(Context context, Uri uri, Messenger messenger) {
        sendData(downloadData(context, uri), messenger);
    }

    /**
     *	Use the provided Messenger to send a Message to a Handler in another process.
     *
     * 	The provided string, outputData, should be put into a Bundle
     * 	and included with the sent Message.
     */
    private static void sendData(String outputData, Messenger messenger) {
        Message msg = Message.obtain();
        Bundle data = new Bundle();
        data.putString(DOWNLOADED_DATA_KEY, outputData);

        // Make the Bundle the "data" of the Message.
        msg.setData(data);

        try {
            // Send the Message back to the client Activity.
            messenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Download the data located at the provided internet url using the URL class,
     * return the downloaded data.
     *
     * @param context	the context
     * @param uri       the web url
     *
     * @return          the downloaded data
     */
    public static String downloadData(Context context, Uri uri) {
        return /*HTTPHelper.getData(uri.toString())*/null;
    }
}