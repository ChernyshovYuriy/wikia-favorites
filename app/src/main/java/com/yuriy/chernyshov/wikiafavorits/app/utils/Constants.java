package com.yuriy.chernyshov.wikiafavorits.app.utils;

/**
 * Created at Android Studio
 * Author: Yuriy Chernyshov
 * Date: 01.07.14
 * Time: 22:09
 */
public class Constants {

    public static final String API_METHOD_KEY = "API_METHOD_KEY";
    public static final String BATCH_ID_KEY = "BATCH_ID_KEY";
    public static final String GET_LIST_RESPONSE_ITEM_KEY = "GET_LIST_RESPONSE_ITEM_KEY";

    public static final int GET_LIST_RESPONSE_MESSENGER_KEY = 1;
}