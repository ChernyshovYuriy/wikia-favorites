package com.yuriy.chernyshov.wikiafavorits.app.net;

import android.net.Uri;

/**
 * Created at Android Studio
 * Author: Yuriy Chernyshov
 * Date: 01.07.14
 * Time: 22:03
 */
public interface Downloader {

    public String downloadFromUri(Uri uri);
}