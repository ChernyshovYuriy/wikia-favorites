package com.yuriy.chernyshov.wikiafavorits.app.api;

/**
 * Created at Android Studio
 * Author: Yuriy Chernyshov
 * Date: 01.07.14
 * Time: 22:56
 */
public enum  APIMethod {
    
    GET_LIST,
    GET_DETAILS;

    /**
     * Convert enum value into String
     * @param value Enum value
     * @return String value of the provided enum value
     */
    public static String stringValueOf(APIMethod value) {
        switch (value) {
            case GET_DETAILS:
                return "getDetails";
            case GET_LIST:
                return "getList";
        }
        return "";
    }

    @Override
    public String toString() {
        return super.toString();
    }
}