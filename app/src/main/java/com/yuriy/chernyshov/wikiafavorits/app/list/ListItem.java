package com.yuriy.chernyshov.wikiafavorits.app.list;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 5/16/14
 * Time: 9:31 PM
 */

import java.io.Serializable;

/**
 * This class describes and holds all fields of the List Item object
 */
public class ListItem implements Serializable {

    private String mTitle = "";
    private String mURL = "";
    private String mImgURI = "";
    private String mName = "";
    private String mHub = "";
    private String mTopic = "";
    private String mDomain = "";
    private String mWordmark = "";
    private String mHeadline = "";
    private String mDesc = "";
    private String mImage = "";
    private String mImageThumb = "";
    private int mId;

    /**
     * Getters and Setters of all object's fields
     */

    public int getId() {
        return mId;
    }

    public void setId(int value) {
        mId = value;
    }

    public String getImageThumb() {
        return mImageThumb;
    }

    public void setImageThumb(String value) {
        mImageThumb = value;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String value) {
        mImage = value;
    }

    public String getHeadline() {
        return mHeadline;
    }

    public void setHeadline(String value) {
        mHeadline = value;
    }

    public String getDesc() {
        return mDesc;
    }

    public void setDesc(String value) {
        mDesc = value;
    }

    public String getWordmark() {
        return mWordmark;
    }

    public void setWordmark(String value) {
        mWordmark = value;
    }

    public String getDomain() {
        return mDomain;
    }

    public void setDomain(String value) {
        mDomain = value;
    }

    public String getTopic() {
        return mTopic;
    }

    public void setTopic(String value) {
        mTopic = value;
    }

    public String getHub() {
        return mHub;
    }

    public void setHub(String value) {
        mHub = value;
    }

    public String getName() {
        return mName;
    }

    public void setName(String value) {
        mName = value;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String value) {
        mTitle = value;
    }

    public String getURL() {
        return mURL;
    }

    public void setURL(String value) {
        mURL = value;
    }

    public String getImgURI() {
        return mImgURI;
    }

    public void setImgURI(String value) {
        mImgURI = value;
    }
}