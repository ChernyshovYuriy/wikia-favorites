package com.yuriy.chernyshov.wikiafavorits.app.api;

/**
 * Created at Android Studio
 * Author: Yuriy Chernyshov
 * Date: 01.07.14
 * Time: 22:51
 */
public enum APIController {

    WIKIS_API;

    /**
     * Convert enum value into String
     * @param value Enum value
     * @return String value of the provided enum value
     */
    public static String stringValueOf(APIController value) {
        switch (value) {
            case WIKIS_API:
                return "WikisApi";
        }
        return "";
    }

    @Override
    public String toString() {
        return super.toString();
    }
}