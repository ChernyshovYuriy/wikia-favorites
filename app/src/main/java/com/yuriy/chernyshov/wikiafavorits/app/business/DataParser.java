package com.yuriy.chernyshov.wikiafavorits.app.business;

import android.util.Log;

import com.yuriy.chernyshov.wikiafavorits.app.list.ListItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 5/17/14
 * Time: 5:29 PM
 */

/**
 *
 * THIS IS OBSOLETE CLASS, IT IS NOT IN USE ANYMORE
 *
 * This class provided functionality to parse JSON data
 */
public class DataParser {

    @SuppressWarnings("unused")
    private static final String LOG_TAG = DataParser.class.getSimpleName();

    /**
     * Define JSON data keys
     */
    private static final String KEY_BATCHES = "batches";
    private static final String KEY_CURRENT_BATCH = "currentBatch";
    private static final String KEY_ITEMS = "items";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_HUB = "hub";
    private static final String KEY_LANGUAGE = "language";
    private static final String KEY_TOPIC = "topic";
    private static final String KEY_DOMAIN = "domain";
    private static final String KEY_WORDMARK = "wordmark";
    private static final String KEY_TITLE = "title";
    private static final String KEY_URL = "url";
    private static final String KEY_STATS = "stats";
    private static final String KEY_EDITS = "edits";
    private static final String KEY_ARTICLES = "articles";
    private static final String KEY_PAGES = "pages";
    private static final String KEY_USERS = "users";
    private static final String KEY_ACTIVE_USERS = "activeUsers";
    private static final String KEY_IMAGES = "images";
    private static final String KEY_VIDEOS = "videos";
    private static final String KEY_ADMINS = "admins";
    private static final String KEY_TOP_USERS = "topUsers";
    private static final String KEY_HEAD_LINE = "headline";
    private static final String KEY_LANG = "lang";
    private static final String KEY_FLAGS = "flags";
    private static final String KEY_DESC = "desc";
    private static final String KEY_IMAGE = "image";
    private static final String KEY_WAM_SCORE = "wam_score";
    private static final String KEY_ORIGINAL_DIMENSIONS = "original_dimensions";
    private static final String KEY_WIDTH = "width";
    private static final String KEY_HEIGHT = "height";

    /**
     * Return the number of batches in the provided search criteria
     * @param jsonObject {@link org.json.JSONObject} object
     * @return number of batches or 1 is there were an error while processing data
     * @throws JSONException
     */
    public static int parseBatchesNumber(JSONObject jsonObject) throws JSONException {
        int batchesNumber = 1;
        if (jsonObject == null) {
            return batchesNumber;
        }
        if (jsonObject.has(KEY_BATCHES)) {
            return jsonObject.getInt(KEY_BATCHES);
        }
        return batchesNumber;
    }

    /**
     * Return the current processed batch Id
     * @param jsonObject JSON object
     * @return Id of the current processed batch or 0 if there were an error while processing data
     * @throws JSONException
     */
    public static int parseCurrentBatch(JSONObject jsonObject) throws JSONException {
        int currentBatch = 0;
        if (jsonObject == null) {
            return currentBatch;
        }
        if (jsonObject.has(KEY_CURRENT_BATCH)) {
            return jsonObject.getInt(KEY_CURRENT_BATCH);
        }
        return currentBatch;
    }

    /**
     * Return JSONArray of the data items
     * @param jsonObject JSON object
     * @return {@link org.json.JSONArray} of the data items
     * @throws JSONException
     */
    public static JSONArray parseItemsArray(JSONObject jsonObject) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        if (jsonObject.has(KEY_ITEMS)) {
            jsonArray = jsonObject.getJSONArray(KEY_ITEMS);
        }
        return jsonArray;
    }

    /**
     * Return JSONArray of the Details items for the specified array of Id's
     * @param jsonObject JSON object
     * @param ids        array of the processing items Id's
     * @return {@link org.json.JSONArray} of the Details items for the specified array of Id's
     * @throws JSONException
     */
    public JSONArray parseDetailsItemsArray(JSONObject jsonObject, int[] ids) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        if (!jsonObject.has(KEY_ITEMS)) {
            return jsonArray;
        }
        JSONObject object = (JSONObject) jsonObject.get(KEY_ITEMS);
        for (int id: ids) {
            String key = String.valueOf(id);
            if (object.has(key)) {
                jsonArray.put(object.get(key));
            }
        }
        return jsonArray;
    }

    /**
     * Return collection of the {@link com.yuriy.chernyshov.wikiafavorits.app.list.ListItem} objects
     * @param jsonArray {@link org.json.JSONArray} to process to
     * @return collection of the parsed {@link com.yuriy.chernyshov.wikiafavorits.app.list.ListItem}
     * @throws JSONException
     */
    public static List<ListItem> parseItemsFromArray(JSONArray jsonArray) throws JSONException {
        List<ListItem> listItems = new ArrayList<ListItem>();
        if (jsonArray == null) {
            return listItems;
        }
        int length = jsonArray.length();
        JSONObject item;
        ListItem listItem;
        for (int i = 0; i < length; i++) {
            item = jsonArray.getJSONObject(i);
            listItem = parseItemFromArray(item);
            listItems.add(listItem);
        }
        return listItems;
    }

    /**
     * Return collection of the {@link com.yuriy.chernyshov.wikiafavorits.app.list.ListItem}'s
     * objects with parsed Image thumbs URL's
     * @param jsonArray JSONArray to process to
     * @return collection of the {@link com.yuriy.chernyshov.wikiafavorits.app.list.ListItem}'s
     * objects with parsed image thumbs URL's
     * @throws JSONException
     */
    public List<ListItem> parseImageThumbsFromArray(JSONArray jsonArray) throws JSONException {
        List<ListItem> listItems = new ArrayList<ListItem>();
        if (jsonArray == null) {
            return listItems;
        }
        int length = jsonArray.length();
        JSONObject item;
        ListItem listItem;
        for (int i = 0; i < length; i++) {
            item = jsonArray.getJSONObject(i);
            listItem = parseImageThumbFromArray(item);
            listItems.add(listItem);
        }
        return listItems;
    }

    /**
     * Convert raw data into {@link org.json.JSONObject}
     *
     * @param rawData raw data as String object which is downloaded from the server
     *
     * @return a {@link org.json.JSONObject} converted from {@link java.lang.String}
     */
    public static JSONObject getJSONfromRawData(String rawData) {
        JSONObject jsonObject = new JSONObject();
        if (rawData == null) {
            Log.w(LOG_TAG, "Raw data is null");
            return jsonObject;
        }

        if (rawData.equals("")) {
            Log.w(LOG_TAG, "Raw data is empty");
            return jsonObject;
        }

        try {
            jsonObject = new JSONObject(rawData);
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Create JSON error:" + e.getMessage());
        }
        return jsonObject;
    }

    /**
     * Return {@link com.yuriy.chernyshov.wikiafavorits.app.list.ListItem} object with parsed
     * value of the Image thumb URL
     * @param jsonObject {@link org.json.JSONObject} object
     * @return {@link com.yuriy.chernyshov.wikiafavorits.app.list.ListItem} object with parsed
     * value of the Image thumb URL
     * @throws JSONException
     */
    private ListItem parseImageThumbFromArray(JSONObject jsonObject) throws JSONException {
        ListItem item = new ListItem();
        if (jsonObject == null) {
            return item;
        }
        if (jsonObject.has(KEY_ID)) {
            item.setId(jsonObject.getInt(KEY_ID));
        }
        if (jsonObject.has(KEY_IMAGE)) {
            item.setImageThumb(jsonObject.getString(KEY_IMAGE));
        }
        return item;
    }

    /**
     * Return {@link com.yuriy.chernyshov.wikiafavorits.app.list.ListItem} object with all necessary
     * fields filled in
     * @param jsonObject {@link org.json.JSONObject} object
     * @return {@link com.yuriy.chernyshov.wikiafavorits.app.list.ListItem} object with all necessary
     * fields filled in
     * @throws JSONException
     */
    private static ListItem parseItemFromArray(JSONObject jsonObject) throws JSONException {
        ListItem item = new ListItem();
        if (jsonObject == null) {
            return item;
        }
        if (jsonObject.has(KEY_ID)) {
            item.setId(jsonObject.getInt(KEY_ID));
        }
        if (jsonObject.has(KEY_NAME)) {
            item.setName(jsonObject.getString(KEY_NAME));
        }
        if (jsonObject.has(KEY_HUB)) {
            item.setHub(jsonObject.getString(KEY_HUB));
        }
        if (jsonObject.has(KEY_TOPIC)) {
            item.setTopic(jsonObject.getString(KEY_TOPIC));
        }
        if (jsonObject.has(KEY_DOMAIN)) {
            item.setDomain(jsonObject.getString(KEY_DOMAIN));
        }
        if (jsonObject.has(KEY_WORDMARK)) {
            item.setWordmark(jsonObject.getString(KEY_WORDMARK));
        }
        if (jsonObject.has(KEY_TITLE)) {
            item.setTitle(jsonObject.getString(KEY_TITLE));
        }
        if (jsonObject.has(KEY_URL)) {
            item.setURL(jsonObject.getString(KEY_URL));
        }
        if (jsonObject.has(KEY_HEAD_LINE)) {
            item.setHeadline(jsonObject.getString(KEY_HEAD_LINE));
        }
        if (jsonObject.has(KEY_DESC)) {
            item.setDesc(jsonObject.getString(KEY_DESC));
        }
        if (jsonObject.has(KEY_IMAGE)) {
            item.setImage(jsonObject.getString(KEY_IMAGE));
        }
        return item;
    }
}