package com.yuriy.chernyshov.wikiafavorits.app.list;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.yuriy.chernyshov.wikiafavorits.app.R;
import com.yuriy.chernyshov.wikiafavorits.app.utils.ImageFetcher;

import java.util.Hashtable;
import java.util.List;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 5/16/14
 * Time: 8:52 PM
 */

/**
 * This class describes adapter for the List View, holds content view and collection of the data.
 */
public class ListAdapter extends BaseAdapter {

    @SuppressWarnings("unused")
    private final static String LOG_TAG = ListAdapter.class.getSimpleName();

    private ListAdapterViewHolder mViewHolder;
    private Activity mCurrentActivity;
    private ImageFetcher mImageFetcher;
    private final ListAdapterData<ListItem> mAdapterData = new ListAdapterData<ListItem>(null);
    private final Hashtable<Integer, Integer> mItemsIdsHash = new Hashtable<Integer, Integer>();

    /**
     * Constructor
     * @param activity     current {@link android.app.Activity}
     * @param imageFetcher {@link com.yuriy.chernyshov.wikiafavorits.app.utils.ImageFetcher} instance
     */
    public ListAdapter(Activity activity, ImageFetcher imageFetcher) {
        mCurrentActivity = activity;
        mImageFetcher = imageFetcher;
    }

    @Override
    public int getCount() {
        return mAdapterData.getItemsCount();
    }

    @Override
    public Object getItem(int position) {
        return mAdapterData.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ListItem listItem = (ListItem) getItem(position);
        convertView = prepareViewAndHolder(convertView, R.layout.list_item);
        mViewHolder.mTitleView.setText(listItem.getTitle());
        mViewHolder.mURLView.setText(listItem.getURL());

        if (!listItem.getImage().equals("")) {
            // Load the image asynchronously into the ImageView, this also takes care of
            // setting a placeholder image while the background thread runs
            mImageFetcher.loadImage(listItem.getImage(), mViewHolder.mImgView);
        }
        return convertView;
    }

    public ListAdapterData<ListItem> getData() {
        return mAdapterData;
    }

    /**
     * Return {@link com.yuriy.chernyshov.wikiafavorits.app.list.ListItem} by the provided Id
     * @param itemId Id of the item
     * @return {@link com.yuriy.chernyshov.wikiafavorits.app.list.ListItem}
     */
    public ListItem getItemById(int itemId) {
        int position = mItemsIdsHash.get(itemId);
        return mAdapterData.getItem(position);
    }

    /**
     * Add {@link com.yuriy.chernyshov.wikiafavorits.app.list.ListItem} into collection
     * @param value {@link com.yuriy.chernyshov.wikiafavorits.app.list.ListItem}
     */
    public void addItem(ListItem value) {
        mAdapterData.addItem(value);
        mItemsIdsHash.put(value.getId(), mAdapterData.getItemsCount() - 1);
    }

    public void addItems(List<ListItem> items) {
        for (ListItem item : items) {
            addItem(item);
        }
    }

    /**
     * Prepare view holder for the item rendering
     * @param convertView      {@link android.view.View} associated with List Item
     * @param inflateViewResId Id of the View layout
     * @return
     */
    private View prepareViewAndHolder(View convertView, int inflateViewResId) {
        // If there is no View created - create it here and set it's Tag
        if (convertView == null) {
            convertView = LayoutInflater.from(mCurrentActivity).inflate(inflateViewResId, null);
            mViewHolder = createViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            // Get View by provided Tag
            mViewHolder = (ListAdapterViewHolder) convertView.getTag();
        }
        return convertView;
    }

    /**
     * Create View holder to keep reference tot he layout items
     * @param view {@link android.view.View}
     * @return {@link com.yuriy.chernyshov.wikiafavorits.app.list.ListAdapterViewHolder} object
     */
    private ListAdapterViewHolder createViewHolder(View view) {
        ListAdapterViewHolder viewHolder = new ListAdapterViewHolder();
        viewHolder.mTitleView = (TextView) view.findViewById(R.id.title_view);
        viewHolder.mURLView = (TextView) view.findViewById(R.id.url_view);
        viewHolder.mImgView = (ImageView) view.findViewById(R.id.img_view);
        return viewHolder;
    }
}