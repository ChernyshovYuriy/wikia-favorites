package com.yuriy.chernyshov.wikiafavorits.app.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.yuriy.chernyshov.wikiafavorits.app.api.APIController;
import com.yuriy.chernyshov.wikiafavorits.app.api.APILang;
import com.yuriy.chernyshov.wikiafavorits.app.api.APIMethod;
import com.yuriy.chernyshov.wikiafavorits.app.api.APIServiceProvider;
import com.yuriy.chernyshov.wikiafavorits.app.api.APIServiceProviderImpl;
import com.yuriy.chernyshov.wikiafavorits.app.business.GetListResponseItem;
import com.yuriy.chernyshov.wikiafavorits.app.api.Hub;
import com.yuriy.chernyshov.wikiafavorits.app.net.RequestParams;
import com.yuriy.chernyshov.wikiafavorits.app.net.Downloader;
import com.yuriy.chernyshov.wikiafavorits.app.net.DownloaderImpl;
import com.yuriy.chernyshov.wikiafavorits.app.parser.DataParser;
import com.yuriy.chernyshov.wikiafavorits.app.parser.JSONDataParserImpl;
import com.yuriy.chernyshov.wikiafavorits.app.utils.Constants;
import com.yuriy.chernyshov.wikiafavorits.app.utils.DownloadUtils;

/**
 * Created at Android Studio
 * Author: Yuriy Chernyshov
 * Date: 29.06.14
 * Time: 17:49
 */
public class DownloaderService extends IntentService {

    private static final String LOG_TAG = DownloaderService.class.getSimpleName();

    public DownloaderService() {
        super("DownloaderService");
    }

    public DownloaderService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Messenger messenger = intent.getParcelableExtra(DownloadUtils.MESSENGER_KEY);
        if (messenger == null) {
            return;
        }

        APIMethod apiMethod = (APIMethod) intent.getSerializableExtra(Constants.API_METHOD_KEY);
        RequestParams requestParams = new RequestParams();

        if (apiMethod == null) {
            // TODO: Log here a warning message
            return;
        }

        requestParams.setBatchId(intent.getIntExtra(Constants.BATCH_ID_KEY, 1));
        requestParams.setController(APIController.WIKIS_API);
        requestParams.setHub(Hub.GAMING);
        requestParams.setLang(APILang.ENG);

        if (apiMethod == APIMethod.GET_LIST) {
            requestParams.setMethod(APIMethod.GET_LIST);
            requestParams.setDoExpand(true);
        }

        Downloader downloader = new DownloaderImpl();
        DataParser dataParser = new JSONDataParserImpl();

        APIServiceProvider serviceProvider = new APIServiceProviderImpl(dataParser);
        GetListResponseItem responseItem = serviceProvider.getList(downloader, requestParams);

        sendData(responseItem, messenger, Constants.GET_LIST_RESPONSE_MESSENGER_KEY);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.d(LOG_TAG, "On Destroy");
    }

    /**
     *	Use the provided Messenger to send a Message to a Handler in another process.
     *
     * 	The provided string, outputData, should be put into a Bundle
     * 	and included with the sent Message.
     */
    private static void sendData(GetListResponseItem outputData, Messenger messenger, int what) {
        Message msg = Message.obtain();
        Bundle data = new Bundle();
        data.putSerializable(Constants.GET_LIST_RESPONSE_ITEM_KEY, outputData);

        // Make the Bundle the "data" of the Message.
        msg.setData(data);
        msg.what = what;

        try {
            // Send the Message back to the client Activity.
            messenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Make an intent that will start this service if supplied to startService() as a parameter.
     *
     * @param context		The context of the calling component.
     * @param handler		The handler that the service should use to respond with a result
     *
     * This method utilizes the Factory Method makeMessengerIntent() from the DownloadUtils class.
     * The returned intent is a Command in the Command Processor Pattern. The intent contains a
     * messenger, which plays the role of Proxy in the Active Object Pattern.
     */
    public static Intent makeIntent(Context context, Handler handler) {
        return DownloadUtils.makeMessengerIntent(context, DownloaderService.class, handler);
    }
}