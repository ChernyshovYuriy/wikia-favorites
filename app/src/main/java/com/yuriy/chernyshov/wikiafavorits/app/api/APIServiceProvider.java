package com.yuriy.chernyshov.wikiafavorits.app.api;

import com.yuriy.chernyshov.wikiafavorits.app.business.GetListResponseItem;
import com.yuriy.chernyshov.wikiafavorits.app.net.RequestParams;
import com.yuriy.chernyshov.wikiafavorits.app.net.Downloader;

import java.util.List;

/**
 * Created at Android Studio
 * Author: Yuriy Chernyshov
 * Date: 01.07.14
 * Time: 20:45
 */
public interface APIServiceProvider {

    public GetListResponseItem getList(Downloader downloader, RequestParams requestParams);

    public List getDetails(Downloader downloader, RequestParams requestParams);
}