package com.yuriy.chernyshov.wikiafavorits.app.list;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 5/16/14
 * Time: 9:06 PM
 */
public class ListAdapterViewHolder {

    TextView mTitleView;
    TextView mURLView;
    ImageView mImgView;
}