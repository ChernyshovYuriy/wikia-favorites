package com.yuriy.chernyshov.wikiafavorits.app.net;

import com.yuriy.chernyshov.wikiafavorits.app.api.APIController;
import com.yuriy.chernyshov.wikiafavorits.app.api.APILang;
import com.yuriy.chernyshov.wikiafavorits.app.api.APIMethod;
import com.yuriy.chernyshov.wikiafavorits.app.api.Hub;

/**
 * Created at Android Studio
 * Author: Yuriy Chernyshov
 * Date: 01.07.14
 * Time: 20:50
 */
public class RequestParams {

    private APIController controller;
    private APIMethod method;
    private Hub hub;
    private APILang lang;
    private int batchId;
    private boolean doExpand;

    public APIController getController() {
        return controller;
    }

    public void setController(APIController controller) {
        this.controller = controller;
    }

    public APIMethod getMethod() {
        return method;
    }

    public void setMethod(APIMethod method) {
        this.method = method;
    }

    public Hub getHub() {
        return hub;
    }

    public void setHub(Hub hub) {
        this.hub = hub;
    }

    public APILang getLang() {
        return lang;
    }

    public void setLang(APILang lang) {
        this.lang = lang;
    }

    public int getBatchId() {
        return batchId;
    }

    public void setBatchId(int batchId) {
        this.batchId = batchId;
    }

    public boolean isDoExpand() {
        return doExpand;
    }

    public void setDoExpand(boolean doExpand) {
        this.doExpand = doExpand;
    }
}