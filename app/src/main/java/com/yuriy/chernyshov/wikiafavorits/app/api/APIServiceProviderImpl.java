package com.yuriy.chernyshov.wikiafavorits.app.api;

import android.net.Uri;

import com.yuriy.chernyshov.wikiafavorits.app.business.GetListResponseItem;
import com.yuriy.chernyshov.wikiafavorits.app.net.Downloader;
import com.yuriy.chernyshov.wikiafavorits.app.net.RequestParams;
import com.yuriy.chernyshov.wikiafavorits.app.net.URLBuilder;
import com.yuriy.chernyshov.wikiafavorits.app.parser.DataParser;

import java.util.List;

/**
 * Created at Android Studio
 * Author: Yuriy Chernyshov
 * Date: 01.07.14
 * Time: 20:53
 */
public class APIServiceProviderImpl implements APIServiceProvider {

    @SuppressWarnings("unused")
    private static final String LOG_TAG = APIServiceProviderImpl.class.getSimpleName();

    private DataParser dataParser;

    public APIServiceProviderImpl(DataParser dataParser) {
        this.dataParser = dataParser;
    }

    @Override
    public GetListResponseItem getList(Downloader downloader, RequestParams requestParams) {
        // Build Get List URL
        URLBuilder urlBuilder = new URLBuilder();
        urlBuilder.setLang(APILang.stringValueOf(requestParams.getLang()));
        urlBuilder.setController(APIController.stringValueOf(requestParams.getController()));
        urlBuilder.setBatchId(requestParams.getBatchId());
        urlBuilder.setDoExpand(requestParams.isDoExpand());
        urlBuilder.setMethod(APIMethod.stringValueOf(requestParams.getMethod()));
        urlBuilder.setHub(Hub.stringValueOf(requestParams.getHub()));

        String response = downloader.downloadFromUri(Uri.parse(urlBuilder.buildURL()));

        GetListResponseItem responseItem = new GetListResponseItem();

        responseItem.setCurrentBatch(dataParser.parseCurrentBatch(response));
        responseItem.setBatchesNumber(dataParser.parseBatchesNumber(response));
        responseItem.setItems(dataParser.parseItemsFromArray(response));

        return responseItem;
    }

    @Override
    public List getDetails(Downloader downloader, RequestParams requestParams) {
        return null;
    }
}