package com.yuriy.chernyshov.wikiafavorits.app.parser;

import android.util.Log;

import com.yuriy.chernyshov.wikiafavorits.app.list.ListItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created at Android Studio
 * Author: Yuriy Chernyshov
 * Date: 01.07.14
 * Time: 23:01
 */
public class JSONDataParserImpl extends DataParser {

    @SuppressWarnings("unused")
    protected static final String LOG_TAG = JSONDataParserImpl.class.getSimpleName();

    /**
     * Return the number of batches in the provided search criteria
     * @param inputData input data as String
     * @return number of batches or 1 is there were an error while processing data
     */
    @Override
    public int parseBatchesNumber(String inputData) {
        int batchesNumber = 1;
        JSONObject jsonObject = getJSONfromRawData(inputData);
        if (jsonObject == null) {
            return batchesNumber;
        }
        if (jsonObject.has(KEY_BATCHES)) {
            try {
                return jsonObject.getInt(KEY_BATCHES);
            } catch (JSONException e) {
                Log.w(LOG_TAG, "Can not parse batches number:" + e.getMessage());
            }
        }
        return batchesNumber;
    }

    /**
     * Return the current processed batch Id
     * @param inputData input data as String
     * @return Id of the current processed batch or 0 if there were an error while processing data
     */
    @Override
    public int parseCurrentBatch(String inputData) {
        int currentBatch = 0;
        JSONObject jsonObject = getJSONfromRawData(inputData);
        if (jsonObject == null) {
            return currentBatch;
        }
        if (jsonObject.has(KEY_CURRENT_BATCH)) {
            try {
                return jsonObject.getInt(KEY_CURRENT_BATCH);
            } catch (JSONException e) {
                Log.w(LOG_TAG, "Can not parse current batch:" + e.getMessage());
            }
        }
        return currentBatch;
    }

    @Override
    public List<ListItem> parseItemsFromArray(String inputData) {
        List<ListItem> listItems = new ArrayList<ListItem>();
        JSONObject jsonObject = getJSONfromRawData(inputData);
        JSONArray jsonArray = null;
        try {
            jsonArray = parseItemsArray(jsonObject);
        } catch (JSONException e) {
            Log.w(LOG_TAG, "Can not parse items array:" + e.getMessage());
        }
        if (jsonArray == null) {
            return listItems;
        }
        int length = jsonArray.length();
        JSONObject item = null;
        ListItem listItem;
        for (int i = 0; i < length; i++) {
            try {
                item = jsonArray.getJSONObject(i);
                listItem = parseItemFromArray(item);
                listItems.add(listItem);
            } catch (JSONException e) {
                Log.w(LOG_TAG, "Can not parse item from array:" + e.getMessage());
            }
        }
        return listItems;
    }

    /**
     * Return {@link com.yuriy.chernyshov.wikiafavorits.app.list.ListItem} object with all necessary
     * fields filled in
     * @param jsonObject {@link org.json.JSONObject} object
     * @return {@link com.yuriy.chernyshov.wikiafavorits.app.list.ListItem} object with all necessary
     * fields filled in
     * @throws JSONException
     */
    private ListItem parseItemFromArray(JSONObject jsonObject) throws JSONException {
        ListItem item = new ListItem();
        if (jsonObject == null) {
            return item;
        }
        if (jsonObject.has(KEY_ID)) {
            item.setId(jsonObject.getInt(KEY_ID));
        }
        if (jsonObject.has(KEY_NAME)) {
            item.setName(jsonObject.getString(KEY_NAME));
        }
        if (jsonObject.has(KEY_HUB)) {
            item.setHub(jsonObject.getString(KEY_HUB));
        }
        if (jsonObject.has(KEY_TOPIC)) {
            item.setTopic(jsonObject.getString(KEY_TOPIC));
        }
        if (jsonObject.has(KEY_DOMAIN)) {
            item.setDomain(jsonObject.getString(KEY_DOMAIN));
        }
        if (jsonObject.has(KEY_WORDMARK)) {
            item.setWordmark(jsonObject.getString(KEY_WORDMARK));
        }
        if (jsonObject.has(KEY_TITLE)) {
            item.setTitle(jsonObject.getString(KEY_TITLE));
        }
        if (jsonObject.has(KEY_URL)) {
            item.setURL(jsonObject.getString(KEY_URL));
        }
        if (jsonObject.has(KEY_HEAD_LINE)) {
            item.setHeadline(jsonObject.getString(KEY_HEAD_LINE));
        }
        if (jsonObject.has(KEY_DESC)) {
            item.setDesc(jsonObject.getString(KEY_DESC));
        }
        if (jsonObject.has(KEY_IMAGE)) {
            item.setImage(jsonObject.getString(KEY_IMAGE));
        }
        return item;
    }

    /**
     * Convert raw data into {@link org.json.JSONObject}
     *
     * @param rawData raw data as String object which is downloaded from the server
     *
     * @return a {@link org.json.JSONObject} converted from {@link java.lang.String}
     */
    private JSONObject getJSONfromRawData(String rawData) {
        JSONObject jsonObject = new JSONObject();
        if (rawData == null) {
            Log.w(LOG_TAG, "Raw data is null");
            return jsonObject;
        }

        if (rawData.equals("")) {
            Log.w(LOG_TAG, "Raw data is empty");
            return jsonObject;
        }

        try {
            jsonObject = new JSONObject(rawData);
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Create JSON error:" + e.getMessage());
        }
        return jsonObject;
    }

    /**
     * Return JSONArray of the data items
     * @param jsonObject JSON object
     * @return {@link org.json.JSONArray} of the data items
     * @throws JSONException
     */
    public JSONArray parseItemsArray(JSONObject jsonObject) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        if (jsonObject.has(KEY_ITEMS)) {
            jsonArray = jsonObject.getJSONArray(KEY_ITEMS);
        }
        return jsonArray;
    }
}